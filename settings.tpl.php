<?php

# Turn off all error reporting
ini_set('display_errors', 0);
error_reporting(0);

#
# If everything is fine, all you need to configure for a basic, not so secure
# installation is the database details.
# You may start there and once you feel comfortable, you may adjust
# other aspects of the installation for security.
#

# Declare it as global, but never use it as global
global $config;

################################################################################
#
# Database
#
################################################################################
# Database name
$config['db']['name'] = '';
# Database user name
$config['db']['user'] = '';
# Database password
$config['db']['pwd'] = '';
# Database host
$config['db']['host'] = '';

################################################################################
#
# Core and Modules
#
################################################################################
#
# Paths USE TRAILING SLASHES!!!
# 
# By default I will assume that core modules and contrib are located in the
# same directory as the Api, but they can be placed anywhere else
# This is just for simplicity

$baseDir = __DIR__ . '/';

# Absolute path to the core MyCala_Api installation.
# Is the directory called 'api' just like that.
# Use trailing slash!
$config['modules']['coreInstall'] = $baseDir . 'api/';

# Time in seconds for the lifetime of a session, after this time, the user must
# log back in. 1 hour is default. 0 means ethernal (NOT recomended).
$config['users']['sessionLifetime'] = 0;

# If you want to allow CORS requests
$config['core']['cors'] = true;

# TMP directory, for diverse uses, it will be emptied every now and then
# @todo empty this directory
$config['core']['tmpDir'] = $baseDir . 'tmp/';

# A private token in order to run cron or other protected calls
# Change it!!!!!
// $config['core']['cronToken'] = 'ItIsGoodIfThisIsBigAndHasW3irDLeeT3rsAnd$ymb0lz.IniT';

#####################################################################################
#
# Epost
#
#####################################################################################

# Base domain name eg venatico.com
# You may use localhost or localhost/epost if you are testing inhouse see more bellow.
$config['epost']['thisDomain'] = 'venatico.com';

# When working in localhost I will not use the epost. subdomain
# you can then test with urls such as localhost/epost1 and localhost/epost2
// If you are using 'localDevelop = true' this will be ignored.
$config['epost']['debugLocal'] = false;

# Default subdomain, DO NOT use a final colon
# You may change this to anything you want, but other servers will not be
# able to find you so easily. This is good if you want to have a private system. 
$config['epost']['sub'] = 'epost';

# Force https secure connections?
# If you enable this I will ONLY talk to other ssl enabled hives
# If you set to know then I will use http ALWAYS even if there is the option for https
# this will change when I implement 'settings per hive' and I will know better.
$config['epost']['forceHttps'] = true;

# Server cli path, this is the internal path in your server.
# You do not need to touch this unless you followed the extra security procedures and changed
# the default location of the api.
$config['epost']['serverCli'] = $baseDir . 'cli.php';

##############################################################################
#
# Grace
#
#####################################################################################

# Where do you want me to store the logs? USE TRAILING SLASH!
# Log path. This does not matter if 'logs' is false.
$config['grace']['logPath'] = $baseDir . 'logs/';

# You may also have logs per user in individual folders with their userId
$config['grace']['logPerUser'] = true;

# Set to 'true' if you actually want me to store a log of errors/messages
$config['grace']['logs'] = false;

# Should I display log messages on the screen?
$config['grace']['display'] = false;

# Core modules location, you probably do not need to touch this.
$config['modules']['corePath'] = $config['modules']['coreInstall'] . "modules/";
# Contributed modules, they can stay where they are.
$config['modules']['contribPath'] = $config['modules']['coreInstall'] . "contrib/";
# Resources such as 404 not found images and such, they are not really used at the moment.
$config['core']['resourcesPath'] = $config['modules']['coreInstall'] . 'resources/';

# Location to upload files, USE TRAILING SLASH!!
# Each user will have its own directory within this path
$config['files']['basePath'] = $config['modules']['coreInstall'] . 'files/';

/*******************************************************************************
 * You should not need to touch anything beyond this point.
 */

# List of core modules
$config['modules']['core'] = array('cala','db','users','files','logger');
# List of core modules to load always, you can overide this list
$config['modules']['coreLoad'] = array('cala','db','users','files','logger');

