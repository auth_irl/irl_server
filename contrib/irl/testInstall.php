<?php

/**
 * Test the api installation.
 */
function irl_testInstall()
{

    # All tests
    $allTests = array();

    $allTests['irlsCore'] = array(
        'name' => 'Irls default test',
        'result' =>  true,
        'good' => 'I am here',
        'fail' => 'You should never see this'
    );

    return $allTests;
}
