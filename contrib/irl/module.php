<?php

/** @ingroup GlobalVars
 *  @{
 */

/** @} */
/** @ingroup Constants
 *  @{
 */

/** @} */

/**
 * Boot up procedure
 */
function irl_bootMeUp()
{
    grace_debug("IRL is in the house!");
}


function irl_init()
{
    $paths = array(

        # Generate a new challenge
        # @return error | allGood
        # r=irl_challenge_me
        # @uid the user id
        array(
            'r' => 'irl_challenge_me',
            'action' => 'irl_challengeMe',
            'access' => 'irl_confirmClient',
            'access_params' => array(
                array('key' => 'clientId', 'def' => '', 'req' => true),
                array('key' => 'secretToken', 'def' => '', 'req' => true),
            ),
            'params' => array(
                array("key" => "uid", "def" => "", "req" => true),
                array("key" => "clientId", "def" => "", "req" => true),
            ),
        ),

        # Accept the challenge
        # @return error | allGood
        # r=irl_challenge_accepted
        # @uid the user id
        # @challenge the encrypted challenge
        array(
            'r' => 'irl_challenge_accepted',
            'action' => 'irl_challengeAccepted',
            'access' => 'users_openAccess',
            'params' => array(
                array("key" => "uid", "def" => "", "req" => true),
                array("key" => "challenge", "def" => "", "req" => true),
                array("key" => "clientId", "def" => "", "req" => true),
            ),
        ),

        # Check to see if a challenge was passed
        # @return error | allGood
        # r=irl_challenge_confirm
        # @uid the user id
        # @accessCode the accessCode for the challenge
        array(
            'r' => 'irl_challenge_confirm',
            'action' => 'irl_challengeConfirm',
            'access' => 'irl_confirmClient',
            'access_params' => array(
                array('key' => 'clientId', 'def' => '', 'req' => true),
                array('key' => 'secretToken', 'def' => '', 'req' => true),
            ),
            'params' => array(
                array("key" => "uid", "def" => "", "req" => true),
                array("key" => "accessCode", "def" => "", "req" => true),
            ),
        ),

        # Set a new space for a key
        # @return error | datails to add the key
        # r=irl_open_key_access
        # @uid the user id
        # @accessCode the accessCode for the challenge
        array(
            'r' => 'irl_open_key_access',
            'action' => 'irl_openKeyAccess',
            'access' => 'irl_confirmClient',
            'access_params' => array(
                array('key' => 'clientId', 'def' => '', 'req' => true),
                array('key' => 'secretToken', 'def' => '', 'req' => true),
            ),
            'params' => array(
                array("key" => "uid", "def" => "", "req" => true),
                array("key" => "clientId", "def" => "", "req" => true),
                array("key" => "pubKey", "def" => "000", "req" =>false),
            ),
        ),

        # Register a new key
        # This is for NEW keys only, update is something else
        # @return error | allGood
        # r=irl_register_new_key
        # @key a public key
        # @secret a secret code (numeric) to confirm the request
        # @uid the user id
        array(
            'r' => 'irl_register_new_key',
            'action' => 'irl_registerNewKey',
            'access' => 'users_openAccess',
            'params' => array(
                array("key" => "pubKey",  "def" => "", "req" => true),
                array("key" => "secret", "def" => "", "req" => true),
                array("key" => "uid",    "def" => "", "req" => true),
                array("key" => "clientId", "def" => "", "req" => true),
            ),
        ),

        # Retrieve a public key
        # @return error | public key
        # r=irl_retrieve_key
        # @uid the user id
        array(
            'r' => 'irl_retrieve_key',
            'action' => 'irl_retrieveKey',
            'access' => 'irl_confirmClient',
            'access_params' => array(
                array('key' => 'clientId', 'def' => '', 'req' => true),
                array('key' => 'secretToken', 'def' => '', 'req' => true),
            ),
            'params' => array(
                array('key' => 'uid', 'def' => '', 'req' => true),
                array('key' => 'clientId', 'def' => '', 'req' => true),
            ),
        ),

        # Delete old challenges
        # @return error | true
        # r=irl_delete_old_challenges
        # @timeLimit seconds that the key is valid (optional)
        # @uid the user id (optional)
        array(
            'r' => 'irl_delete_old_challenges',
            'action' => 'irl_deleteOldChallenges',
            'access' => 'irl_confirmClient',
            'access_params' => array(
                array('key' => 'clientId', 'def' => '', 'req' => true),
                array('key' => 'secretToken', 'def' => '', 'req' => true),
            ),
            'params' => array(
                array("key" => "timeLimit", "def" => "60", "req" => false),
                array("key" => "uid", "def" => "", "req" => false),
                array("key" => "clientId", "def" => "", "req" => true),
            ),
        ),

        # Get logs
        # @return error | logs_list
        # r=irl_logs_get
        # @uid the user id
        # @clientId the client id
        # @ini where to start
        # By default I will retrieve 10
        array(
            'r' => 'irl_logs_get',
            'action' => 'irl_logsGet',
            'access' => 'irl_confirmClient',
            'access_params' => array(
                array('key' => 'clientId', 'def' => '', 'req' => true),
                array('key' => 'secretToken', 'def' => '', 'req' => true),
            ),
            'params' => array(
                array("key" => "uid", "def" => "", "req" => true),
                array("key" => "clientId", "def" => "0", "req" => true),
                array("key" => "ini", "def" => "0", "req" => false),
            ),
        ),

    );

    return $paths;
}

/**
 * Register a new table
 */
function irl_registerNewTable($table, $secret, $uid)
{
    grace_debug('A new table will be stored: ' . $table . ' | ' . $secret . ' | ' . $uid);

    # Does it exist already?
    $q = sprintf(
        "SELECT * FROM `irl_tables` WHERE `secretCode` = '%s' AND `idUser` = '%s'",
        $secret,
        $uid
    );

    $userTable = db_querySingle($q);
    if (!$userTable) {
        grace_debug('The table does not seem to exist, I can not update it');
        return IRL_ERROR_GENERAL;
    }
    //print_r($table);
    if ($userTable['numbers'] == '') {
        grace_debug('The table is empty, I can update it');
        $q = sprintf(
            "UPDATE `irl_tables` 
			SET `numbers` = '%s' 
			WHERE `idUser` = '%s' 
			AND `secretCode` = '%s'",
            $table,
            $uid,
            $secret
        );
        db_exec($q);
        return SUCCESS_ALL_GOOD;
    } else {
        grace_debug('This table is full, you need to request an update.');
        return IRL_ERROR_GENERAL;
    }
    return true;
}

/**
 * Retrieve a challenge.
 */
function irl_challengeGet($uid, $accessCode)
{
    grace_debug('Looking for a challenge: ' . $accessCode);

    $q = sprintf(
        "SELECT * FROM `irl_tmpChallenges` WHERE `accessCode` = '%s' AND `idUser` = '%s'",
        $accessCode,
        $uid
    );

    $challenge = db_querySingle($q);

    if (!$challenge) {
        grace_debug('The challenge does not seem to exist.');
        return false;
    }

    return $challenge;
}

/**
 * Register a new key
 * External call
 * @todo add token usage
 */
function irl_registerNewKey($key, $secret, $uid, $clientId)
{
    grace_debug('A new key will be stored: ' .
        $key . ' | ' .
        $secret . ' | ' .
        $uid);

    # Does it exist already?
    $q = sprintf(
        "SELECT * FROM `irl_userKeys` WHERE `secretCode` = '%s' AND `idUser` = '%s' AND `idClient` = '%s'",
        $secret,
        $uid,
        $clientId
    );

    $userKey = db_querySingle($q);

    if (!$userKey) {
        grace_debug('The key does not seem to exist, I can not update it');
        tools_setErrorR();
        return 'ERROR_KEY_DOES_NOT_EXIST';
    }

    if ($userKey['pubKey'] == '000') {
        grace_debug('The key is empty, I can update it');
        $q = sprintf(
            "UPDATE `irl_userKeys` 
			SET `pubKey` = '%s' 
			WHERE `idUser` = '%s' 
			AND `secretCode` = '%s'
			AND `idClient` = '%s'
",
            $key,
            $uid,
            $secret,
            $clientId
            );
        db_exec($q);
        return SUCCESS_ALL_GOOD;
    } else {
        grace_debug('This key is set, you need to request an update.');
        tools_setErrorR();
        return 'ERROR_KEY_IS_SET';
    }
    return true;
}

/**
 * Generates a new challenge.
 *
 * External call.
 */
function irl_challengeMe($uid, $clientId)
{
    grace_debug('I will now generate a new challenge: ' . $clientId);

    # Get the public key for this user
    $pubKey = irl_keyGetForUser($uid, $clientId);

    grace_debug('Got key: ' . JSON_encode($pubKey));
    grace_debug('Public key: ' . $pubKey['pubKey']);

    if (!$pubKey) {
        grace_error('There is no key for this user, I can not work like this.');
        tools_setErrorR();
        return 'IRL_ERROR_NO_KEY';
    }

    /**
     * Combination Replacement
     * The number of ways to choose a sample of r elements from a set of n
     * distinct objects where order does not matter and replacements are
     * allowed.
     * If my math is right 999 posible elements will give 991,025,976 possibilities
     */

    $challenge = '';

    for ($i = 0; $i < 3; $i++) {
        $challenge .= rand(1, 999);
    }

    # @todo this could be more secure.
    $accessCode = rand(100, 100000);

    $q = sprintf(
        "INSERT INTO `irl_tmpChallenges` 
		(`idUser`, `idClient`, `challenge`, `created`, `accessCode`, `passed`)
		VALUES('%s','%s','%s','%s','%s',0)",
        $uid,
        $clientId,
        $challenge,
        time(),
        $accessCode
    );

    db_exec($q);

    $encrypted = irl_encMe($challenge, $pubKey);

    if (!$encrypted) {
        grace_error('No challenge created, maybe the key is invalid');
        tools_setErrorR();
        return 'IRL_ERROR_KEY_ERROR';
    } else {
        grace_debug('The challenge encrypted is: ' . $encrypted);
    }
    return array(
        'challenge' => $encrypted,
        'accessCode' => $accessCode
    );
}

/**
 * Get the key for a user.
 */
function irl_keyGetForUser($uid, $clientId)
{
    grace_debug('Getting the public key for a user');

    $q = sprintf("SELECT * FROM `irl_userKeys` WHERE idUser = '%s' AND idClient = '%s'", $uid, $clientId);

    $key = db_querySingle($q);

    if (!$key) {
        grace_debug('No key found');
        return false;
    } else {
        return $key;
    }
}

/**
 * Challenge accepted.
 */
function irl_challengeAccepted($uid, $solution, $clientId)
{
    grace_debug('Challenge accepted!');

    # Get the challenge and confirm it is valid
    # only the very last one is valid
    # time limit 60 seconds
    # @todo maybe use irl_challengeGet()
    $q = sprintf(
        "SELECT * FROM `irl_tmpChallenges` 
		WHERE `idUser` = '%s'
		AND `idClient` = '%s'
		AND `passed` != 1
		AND created > %s
		ORDER BY created DESC
		LIMIT 1 ",
        $uid,
        $clientId,
        time() - 60
    );

    $challenge = db_querySingle($q);

    if (!$challenge) {
        grace_debug('No challenge found');

        irl_logNew('e', '0', $uid, $clientId, '');
        tools_setErrorR();
        return 'IRL_ERROR_NO_CHALLENGE';
    }

    grace_debug('Found challenge: ' . $challenge['challenge']);

    # Do they match?
    if ($challenge['challenge'] != $solution) {
        grace_debug('The solution is no good');

        # Mark challenge as failed AKA delete
        # This will delete ALL challenges actually
        //$q = sprintf("DELETE FROM `irl_tmpChallenges` WHERE idUser = '%s'", $uid);

        db_exec($q);

        irl_logNew('f', '0', $uid, $clientId, '');
        tools_setErrorR();
        return 'IRL_ERROR_NO_MATCH';
    }
    grace_debug('The solution is good');

    # Mark challenge as passed
    $q = sprintf(
        "UPDATE `irl_tmpChallenges`
		SET `passed` = 1 
		WHERE `idUser` = '%s'
		AND `idClient` = '%s'
		AND `challenge` = '%s'",
        $uid,
        $clientId,
        $solution
    );

    db_exec($q);

    irl_logNew('p', '1', $uid, $clientId, '');
    return SUCCESS_ALL_GOOD;
}

/**
 * Confirm if a challenge was passed.
 * External call
 */
function irl_challengeConfirm($uid, $accessCode)
{
    grace_debug('Confirm challenge.');

    $challenge = irl_challengeGet($uid, $accessCode);

    if (!$challenge) {
        grace_debug('No challenge found');
        tools_setErrorR();
        return 'ERROR_NO_CHALLENGE_FOUND';
    }

    if ($challenge['passed'] == 1) {
        grace_debug('The challenge was passed.');
        return SUCCESS_ALL_GOOD;
    } else {
        grace_debug('Challenge not passed, yet...');
        tools_setErrorR();
        return 'IRL_CHALLENGE_NOT_PASSED';
    }
}

/****************************************************************************
 * Keys
 */

/**
 * Request a new spot for a key.
 *
 * External call
 * If a key is provided then it will be set.
 * It only works via POST because the key has newLines and via GEt it just
 * does not work fine.
 */
function irl_openKeyAccess($uid, $clientId, $key = '000')
{
    grace_debug('Open a new key access');

    # Does it exist already?
    $oldKey = irl_keyGetForUser($uid, $clientId);

    if ($oldKey && $oldKey['pubKey'] != '000') {
        grace_debug('It looks like this key is already set.');
        return tools_setErrorR('The key space is already set', 'IRL_ERROR_KEY_EXISTS');
    }

    $secretCode = rand(100, 100000);

    $q = sprintf(
        "INSERT INTO `irl_userKeys` (`idUser`,`idClient`,`pubKey`,`created`,`secretCode`)
		VALUES('%s','%s','%s','%s','%s')",
        $uid,
        $clientId,
        $key,
        time(),
        $secretCode
    );

    db_exec($q);

    # Now, lets get it
    $key = irl_keyGetForUser($uid, $clientId);

    return $key;
}

/**
 * Retrieves a public key.
 *
 * External call, but can be called internally too.
 * @todo maybe there should be an external call so I can return error in case
 * of no key.
 */
function irl_retrieveKey($uid, $clientId)
{
    grace_debug('Getting the public key of: ' . $uid);

    $key = irl_keyGetForUser($uid, $clientId);

    if (!$key) {
        return tools_setErrorR('There is no key set for this user', 'IRL_ERROR_NO_KEY');
    } elseif ($key['pubKey'] == '000' || $key['pubKey'] == '') {
        return tools_setErrorR('The key space exists, but it is empty or with 000', 'IRL_ERR_NO_KEY');
    }

    return $key;
}

/**
 * Encrypts with user pubKey
 */
function irl_encMe($w, $key)
{
    grace_debug('I will encrypt');

    # Encrypt
    $key = openssl_get_publickey($key['pubKey']);
    openssl_public_encrypt($w, $encrypted, $key);
    $encrypted = base64_encode($encrypted);

    if (!$encrypted) {
        grace_error('Unable to encrypt, wrong key?');
    }

    return $encrypted;
}

/**
 * Delete old challenges.
 * External call, can be local.
 */
function irl_deleteOldChallenges($timeLimit, $uid)
{
    grace_debug('Deleting old challenges, specific user? ' . $uid . ' time limit: ' . $timeLimit);

    $byUser = '';

    if ($uid) {
        $byUser = sprintf(' AND `idUser` = \'%s\'', $uid);
    }

    # Limit of 500 just in case
    # @todo make a configuration for this
    $q = sprintf(
        "DELETE FROM `irl_tmpChallenges` 
		WHERE `created` <= '%s' %s LIMIT 500",
        time() - $timeLimit,
        $byUser
    );

    db_exec($q);

    return true;
}

/****************************************************************************
 * Logs
 */

/**
 * Register a new log entry.
 * @todo add agent, that is not implemented as of now.
 * @type e | f | p
 */
function irl_logNew($type, $status, $uid, $clientId, $agent = '')
{
    grace_debug('Register a new log entry: ' . $type);

    $pubKey = irl_keyGetForUser($uid, $clientId);

    $q = sprintf(
        "INSERT INTO `irl_log` (`uid`, `idClient`, `timestamp`, `ip`, `type`, `status`, `agent`)
		VALUES('%s','%s','%s','%s','%s','%s','%s')",
        $uid,
        $clientId,
        time(),
        irl_encMe($_SERVER['REMOTE_ADDR'], $pubKey),
        $type,
        $status,
        $agent
    );

    db_exec($q);

    return true;
}

/**
 * Get a list of logs.
 * @todo open for actual user to see them.
 */
function irl_logsGet($uid, $clientId, $ini)
{
    grace_debug('Get a list of logs');

    $q = "SELECT * FROM `irl_log`
		WHERE uid = '$uid' AND `idClient` = '$clientId'
		LIMIT $ini, 10";

    $logs = db_q($q);

    if (!$logs) {
        tools_setErrorR();
        return 'IRL_NO_LOGS_FOUND';
    }

    return $logs;
}

/**
 * Confirms that the client is valid.
 */
function irl_confirmClient($clientId, $secretToken)
{
    grace_debug('Confirm client identity');

    # Return err is vague, lets not tell the world that we have an insecure
    # token set or no token at all!
    if (conf_get('cronToken', 'core', '') == '') {
        return tools_errSet('secretCode NOT set, I can not work like this, too insecure.', 'IRL_ERR_NO_TOKEN');
    }

    if ($secretToken != conf_get('cronToken', 'core', '')) {
        return tools_errSet('Unable to confirm identity. You are not who you appear to be!', 'IRL_ERR_TOKEN_WRONG');
    }
    return true;
}
