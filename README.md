# Irl public server

Ingreso Remoto Local (Remote Local Access)

Privacy back in your hands.

# Requirements

You will need MyCala Api, this is just a module for it.

You may have gotten a distribution that already includes everything.
If you see a directory called `api` then you have it.

# Installation instructions

You can follow MyCala's installation instructions that you may have gotten with the distribution or at MyCala's website.

Then, this are the installation instructions for the irl module.

## Database

Load the contents of irl.sql in the same database that you created for MyCala

## Settings

Copy/paste the settings that come with the irl module into the file settings.php that you created for MyCala.
Adjust the values accordingly.


