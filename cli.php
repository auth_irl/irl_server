<?php
/**
 * Use this for CLI
 *
 * You may add #!/bin/php and `chmod +x cli.php` if you want to
 * execute directly.
 * I left it like this so it can be used easier in shared
 * hosting environments.
 *
 * Execute with `php cli.php`
 * avoid `php -f` or `php -q` because you need to pass some
 * arguments to the script.
 */

# Kill POST and GET and PUT
# This should not be accessed that way
unset($_POST);
unset($_GET);
unset($_PUT);

include_once("settings.php");
$corePath = $config['modules']['coreInstall'];
require_once($corePath . "core/boot.php");

$r = boot_engage('cli');

